import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView, TouchableOpacity, BackHandler
} from 'react-native';
import { SearchBar } from 'react-native-elements'
import { Header } from 'react-native-elements'
import Friends from './Friends'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Menu from './Menu'
import Posts from './Posts'
import Meteor, { withTracker } from 'react-native-meteor';
import { Nuggets, MyNotification,TypesNuggets } from './Collection/index'
import { Reservation } from './Collection/index'
import IconBadge from 'react-native-icon-badge';
import MyNotifications from './notification'
 class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      MenuPressed: false,
      HomePressed: true,
      NotificationPressed: false,
      NotificationsCounter: 0
    }

  }
  // componentDidUpdate(prevProps, prevState) {
  //   if (this.state.NotificationsCounter !== prevState.NotificationsCounter) {

  //   Meteor.call("GetcountingNotification", (error, result) => {
  //     console.log("result=", result)
  // this.setState({

  //         NotificationsCounter: result

  //       });
      
  //   })
  // }
  // }
  render() {

console.log(this.props)

    let NuggetList = Nuggets.find()

    const { navigate } = this.props.navigation;
    let sideList = null
    let homeList = null
    let notificationsList = null



    if (this.state.MenuPressed == true) {

      sideList = <Menu user={Meteor.user()} navigation={this.props.navigation} />

    }
    if (this.state.HomePressed == true) {
      homeList = <Posts UserType={Meteor.user().profile.UserType} NuggetsData={NuggetList} navigation={this.props.navigation} />
    }
    if (this.state.NotificationPressed == true) {

      notificationsList = <MyNotifications Counter={this.props.List} navigation={this.props.navigation} />

    }

    return (
      <ScrollView>
        <SearchBar
          lightTheme
          icon={{ type: 'font-awesome', name: 'search' }}
          placeholder='Type Here...' />
        <Header style={{ backgroundColor: "#80ffaa" }}>
          <Icon name="home" size={30} color="#900" onPress={() => this.setState({
            HomePressed: true, MenuPressed: false, NotificationPressed: false
          })} />
          <Icon name="people" size={30} color="#900" />
          {console.log("NotificationsCounter",this.state.NotificationsCounter)}
          <Text style={{ color: '#FFFFFF', fontSize: 20, fontWeight: 'bold' }}>{this.props.Notificationlist.length}
            <Icon name="notifications" size={30} color="#900" onPress={() => this.setState({
              HomePressed: false, MenuPressed: false, NotificationPressed: true
            })} />
          </Text>

          <Icon name="menu" size={30} color="#900" onPress={() => this.setState({
            MenuPressed: true, HomePressed: false, NotificationPressed: false
          })} />
        </Header>
        {sideList}
        {homeList}
        {notificationsList}
      </ScrollView>
    )
  }
}
 const notifyHome= withTracker(props => {//---------->changed 
   const Mynotificationrhandle = Meteor.subscribe('Mynotification')
   const List = MyNotification.find({ TouserID: Meteor.userId() })
 console.log("List",List.length)
  return {
     Notificationlist: List
 }})(Home)
 export default notifyHome