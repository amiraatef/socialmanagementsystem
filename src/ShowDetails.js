import React, { Component } from 'react'
import ReactNative, { 
    AppRegistry,
    findNodeHandle,
    StyleSheet,
    UIManager,
    ScrollView,
    Text,
    View,
    PixelRatio,
    TouchableOpacity,
    BackHandler,
    Image,
    Platform,
    TextInput
  } from 'react-native';

  import Holder from './lib/react-native-draggable-holder'
import { Button } from 'react-native-elements';

export default class ShowDetails extends Component {
    state = {

        ImageSource: null,
        isOpen: false,
        Type:"",
        URL:"",
        text:"",
        postionImage:{
          x:0,offx:0,offy:0,
          y:0
        },
        postionText:{
          x:0,
          offx:0,
          offy:0,
          y:0 
        }
      };

      Measure=()=>
      {
        this.refs.Quote .measure((x,y,width,height, px , py)=>{
          this.setState(prevState => ({
            postionText: {
                ...prevState.postionText,
                offx: px,
                x:x,
                offy:py,
                y:y
            }
        }))
      
        })
  
      }
      componentDidMount()
      {  BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
      handleBackPress = () => {
        console.log("hd",this.props.navigation)
        this.props.navigation.navigate('Home') 
        return true;
      }  
  render() {
     MeasureImage=()=>
  {

    this.refs.image.measure((x,y,width,height, px , py)=>{
      this.setState(prevState => ({
        postionImage: {
            ...prevState.postionImage,
            offx: px,
            x:x,
            offy:py,
            y:y
        }
    }))
    })

  }

  console.log("this.props.navigation.state.params.PositionText.x",this.props.navigation.state.params.PositionText.x)
  console.log("this.props.navigation.state.params.PositionText.offsetX",this.props.navigation.state.params.PositionText.offsetx)
console.log(this.props.navigation.state.params)
  let xx=this.props.navigation.state.params.PositionText.x+this.props.navigation.state.params.PositionText.offsetx
  let yy =this.props.navigation.state.params.PositionText.y+this.props.navigation.state.params.PositionText.offsety
let xxI=this.props.navigation.state.params.PositionImage.x+this.props.navigation.state.params.PositionImage.offsetx
let yyI=this.props.navigation.state.params.PositionImage.y+this.props.navigation.state.params.PositionImage.offsety

console.log(xx,yy,xxI,yyI)
    return (
   <View>
<Button  style={styles.btn} title="Back to Home" onPress={() =>{this.props.navigation.navigate('Home')}}/> 
         <View style={{width:'100%' ,height:150}} >

<Holder offsetX={this.props.navigation.state.params.PositionText.x+this.props.navigation.state.params.PositionText.offsetX}
offsetY={this.props.navigation.state.params.PositionText.y+this.props.navigation.state.params.PositionText.offsetY}
 pressDragRelease={this.Measure}  reverse={false} >
<TextInput
 ref="Quote"
 style={styles.Quote}
           multiline={true}
         onChangeText={(text) => this.setState({ text })}
         value={this.props.navigation.state.params.Text}
/>
</Holder> 
</View>
<View style={{width:'100%' ,height:150}} >

<Holder  offsetX={this.props.navigation.state.params.PositionImage.x+this.props.navigation.state.params.PositionImage.offsetX} offsetY={this.props.navigation.state.params.PositionImage.y+this.props.navigation.state.params.PositionImage.offsetY} reverse={false}  pressDragRelease={MeasureImage} renderShape="image"imageSource={null} >
  <Image  style={{width:300,height:300}} ref="image" source={this.props.navigation.state.params.URL.uri} />
</Holder>
</View  >
       </View>
    )
  }
}
const styles = StyleSheet.create({
Quote: {
    fontStyle: 'italic',
    fontWeight: 'bold',
    borderColor: 'black',
    fontSize: 16,
    borderWidth: 5,
    height: 100,
    width:300,
    borderRadius: 10,
    
    },
    btn:{
      color: 'white',
      flexWrap: 'wrap', 
      flexDirection:'row',
        fontSize: 16,
        justifyContent: 'center',
     alignItems: 'center',
     borderWidth: 5,
     borderRadius: 10,
     marginTop:'90%'
     
     }
  })