import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground, TextInput, TouchableOpacity, AsyncStorage,BackHandler
} from 'react-native';
import RNFS from 'react-native-fs';
import Meteor, { withTracker, MeteorListView, } from 'react-native-meteor';
import { Nuggets ,MyNotification} from './Collection/index'
import { connect } from 'react-redux';

class Login extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      error: ""
    }
  }
  
  // componentWillMount(){
  //   if(Meteor.userId().profile)
  //   {
  //     this.props.navigation.navigate('Home')

  //   }
  // }
  
  
  componentDidMount() {
       BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    componentWillReceiveProps(nextProps){
      console.log(nextProps.currentUser)
      if(nextProps.currentUser){
        // this.props.navigation.navigate('Home')
        this.props.navigation.navigate('HomeScreen')

      }  
    }
  
  handleBackPress = () => {
    console.log("hd",this.props.navigation)
    this.props.navigation.navigate('Login') // works best when the goBack is async
    return true;
  }
  login = () => {
    let email = this.state.email
    let password = this.state.password
    Meteor.loginWithPassword({ email }, password, (err,result) => {

      if (err) {
        console.log(err)
      }
      else{
        if(this.props.currentUser)
        {
          if(this.props.NotifyNumber!=null)
         console.log("this.props.NotifyNumber", this.props.NotifyNumber)
  // this.props.navigation.navigate('Home',{"NotifyNumber":this.props.NotifyNumber})
  this.props.navigation.navigate('HomeScreen')
  
        }

      }

    
    });

   
     


  } 
  


   render() {
  
    const { navigate } = this.props.navigation;

    return (<View style={styles.container}>
      <ImageBackground source={require('./img/WelcomeBG.jpg')} style={styles.BGImage} >
        <View style={styles.content}>
          <Text style={styles.Logo} >Manage Your Content</Text>
          <View style={styles.inputContainer}>
            <TextInput underlineColorAndroid='transparent'
              onChangeText={(value) => this.setState({ email: value })}

              style={styles.input} placeholder='email'>
            </TextInput>
            <TextInput
              ref={(el) => { this.password = el; }}

              underlineColorAndroid='transparent' style={styles.input}
              onChangeText={(password) => this.setState({ password })}
              secureTextEntry={true}
              value={this.state.password}
              placeholder='Password'
            >
            </TextInput>
            <TouchableOpacity style={styles.buttonContainer}   onPress={this.login}>
              <Text style={styles.buttonText}

                onPress={this.login}
              >
                Login
</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('Signup', { name: 'Jane' })} style={styles.buttonContainer}>
              <Text style={styles.buttonText}>
                SignUp
</Text>
            </TouchableOpacity>
          </View>
        {/* <Image style={{width: 100, height: 100}} source={{uri: 'https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&h=350'}}></Image> */}
        </View>
      </ImageBackground>
    </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {

    ...state

  }
}
const mapDispatchersToProps = (dispatcher) => {
  return {
    isDbClicked: (DBclick) => dispatcher({ type: 'dbClicked', value: DBclick }),

  }
}
const tracker= withTracker(props => {//---------->changed

  const currentuserhandle=  Meteor.subscribe('myuser')
  const   Mynotificationhandle=  Meteor.subscribe('  Mynotification')
  const NotifyNum=  MyNotification.find({$and:[{TouserID: Meteor.userId()},{status:"New" }]} )
  console.log("NotifyNum",NotifyNum)
  console.log(NotifyNum.length);
  const names=  Meteor.subscribe('UserNams')
console.log("names",names)
    return {
      currentUser: Meteor.user(),
      names:names,
      NotifyNumber:NotifyNum.length

    }
  })(Login)
export default connect(mapStateToProps, mapDispatchersToProps)(tracker);


const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  BGImage: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center'

  },
  content: {
    alignItems: 'center'
  },
  Logo: {
    color: 'white',
    fontSize: 40
    ,
    fontStyle: 'italic',
    fontWeight: 'bold',
    textShadowColor: '#252525',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 15,
    marginBottom: 20
  },
  inputContainer: {
    margin: 20,
    marginBottom: 0,
    padding: 20,
    paddingBottom: 10,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: 'rgba(255,255,255,0.2)'
  },
  input: {
    fontSize: 16,
    height: 40,
    padding: 10,
    marginBottom: 10,
    backgroundColor: 'rgba(255,255,255,1)'



  },
  buttonContainer: {
    alignSelf: 'stretch',
    margin: 20,
    padding: 20,
    backgroundColor: 'yellow',
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: 'rgb(240,230,140)'


  }
  ,
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center'
  }
})