import React, { Component } from 'react'
import { View, BackHandler, Switch, Text,Button ,StyleSheet,
 } from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import Meteor, { withTracker, MeteorListView, } from 'react-native-meteor';
import TimePicker from 'react-native-simple-time-picker';
import moment from 'moment'

export default class LimitTimesComponent extends Component {

  constructor() {
    super()

  }

  state = {
    alltimesToggle: false,
    SomeTimesToggle: false,
    selectedItems: [],
    selectedHoursFrom: 0,
    selectedMinutesFrom: 0,
    selectedHoursTo:0,selectedMinutesTo:0
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  // componentWillUnmount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  // }

  handleBackPress = () => {
    console.log("hd", this.props.navigation)
    this.props.navigation.navigate('Home', { name: 'Jane' }) // works best when the goBack is async
    return true;
  }
  alltimes = (isOn) => {
    console.log("isOn", isOn)
    this.setState({ alltimesToggle: isOn, SomeTimesToggle: false })
  }
  sometimes = () => {
    this.setState({ SomeTimesToggle: isOn, alltimesToggle: false })

  }


  onSelectedItemsChange = selectedItems => {
    console.log("selectedItems", selectedItems)
    this.setState({ selectedItems });
  };

  render() {
    const { selectedHoursFrom,selectedHoursTo,selectedMinutesTo, selectedMinutesFrom } = this.state;

  const {NuggetDBobj}=  this.props.navigation.state.params
console.log(NuggetDBobj)
console.log(this.props.navigation.state.params.NuggetDBobj)
 const submitNugget=()=>{
let obj={...NuggetDBobj}
obj.AvailableTimes= this.state.selectedItems
// Date.prototype.currentTime = function(){ 
//   return ((this.getHours()>12)?(this.getHours()-12):this.getHours()) +":"+ this.getMinutes()+((this.getHours()>12)?('PM'):'AM'); };
//   var d1= new Date(); 
//   var d2 = new Date();
//   d1.setHours(this.state.selectedHoursFrom);
// d1.setMinutes(this.state.selectedMinutesFrom);
// d2.setHours(this.state.selectedHoursTo);
// d2.setMinutes(this.state.selectedMinutesTo);

// console.log("fromTome",d1.currentTime())
// console.log("fromTome",d2.currentTime())
let fromtime=moment(`${this.state.selectedHoursFrom}:${this.state.selectedMinutesFrom}`,"hh:mm:ss a").toString()
let totime=moment(`${this.state.selectedHoursTo}:${this.state.selectedMinutesTo}`,"hh:mm:ss a").toString()
console.log("fromtime",fromtime)
console.log("totime",totime)


obj.From=fromtime
obj.To=totime

console.log("obj",obj)
 Meteor.call('addNuggets', obj)
 this.props.navigation.navigate('Home', { name: 'Jane' }) 
}
    const items = [{
      id: '1',
      name: 'Saturday',
    },
    {
      id: '2',
      name: 'Sunday',
    },
    {
      id: '3',
      name: 'Monday',
    }, {
      id: '4',
      name: 'Tuesday',
    }, {
      id: '5',
      name: 'Wednesday',
    }, {
      id: '6',
      name: 'Thursday',
    },
    {
      id: '7',
      name: 'Friday',
    },
    ];
    const { selectedItems } = this.state;

    return (
      <View style={{ flex: 1, flexDirection:"column",flexWrap:'wrap',alignContent:"space-between" }}>
      
        <View>
          <Text style={{ color: 'black', fontWeight: '900' }}>all times</Text>
          <Switch onValueChange={() => {
            this.setState({ alltimesToggle: !this.state.alltimesToggle })
          }}

            value={this.state.alltimesToggle}
          />
          <Text style={{ color: 'black', fontWeight: '900' }}>sometimes</Text>
          <Switch onValueChange={() => {
            this.setState({ SomeTimesToggle: !this.state.SomeTimesToggle })
          }}

            value={this.state.SomeTimesToggle}
          />
        </View>
     


{this.state.SomeTimesToggle&&<View>
       <MultiSelect
          hideTags
          items={items}
          uniqueKey="name"
          ref={(component) => {
          this.multiSelect = component
          }}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={this.state.selectedItems}
          selectText="Pick Items"
          searchInputPlaceholderText="Search Items..."
          onChangeInput={(text) => console.log(text)}
          altFontFamily="ProximaNova-Light"
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
          tagTextColor="#CCC"
          selectedItemTextColor="#CCC"
          selectedItemIconColor="#CCC"
          itemTextColor="#000"
          displayKey="name"
          fixedHeight={true}
          searchInputStyle={{ color: '#CCC' }}
          submitButtonColor="#CCC"
          submitButtonText="Submit"

        />
        

</View>

}
<View style={styles.container}>
        <Text> 
          From {selectedHoursFrom}:{selectedMinutesFrom}</Text>
        <TimePicker
          selectedHours={selectedHoursFrom}
          selectedMinutes={selectedMinutesFrom}
          onChange={(hours, minutes) => this.setState({ selectedHoursFrom: hours, selectedMinutesFrom: minutes })}
        />
      </View>
      <View style={styles.container}>
        <Text> To{selectedHoursTo}:{selectedMinutesTo}</Text>
        <TimePicker
          selectedHours={selectedHoursTo}
          selectedMinutes={selectedMinutesTo}
          onChange={(hours, minutes) => this.setState({ selectedHoursTo: hours, selectedMinutesTo: minutes })}
        />
      </View>
  <Button style={styles.btn} title="Save" onPress={submitNugget} />
      </View>
    )
  }
}

const styles= StyleSheet.create({

  btn:{
    color: 'white',
    flexWrap: 'wrap', 
    flexDirection:'row',
      fontSize: 16,
      justifyContent: 'center',
   alignItems: 'center',
   borderWidth: 5,
   borderRadius: 10,
   width:150
   
   
   },  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})