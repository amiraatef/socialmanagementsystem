// @flow
import { StyleSheet } from 'react-native'

const calendarHeight = 2400
// const eventPaddingLeft = 4
const leftMargin = 50 - 1

export default function styleConstructor (
  theme = {}
) {
  let style = {
    container: {
      flex: 1,
      backgroundColor: '#03A9F4',
      ...theme.container
    },
    contentStyle: {
      backgroundColor: 'white',
      height: calendarHeight + 10
    },
    header: {
      paddingHorizontal: 30,
      height: 50,
      borderTopWidth: 2,
      borderBottomWidth: 2,
      borderColor: 'black',
      backgroundColor: '#F5F5F6',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      ...theme.header
    },
    headerText: {
      fontSize: 20,
      fontWeight: 'bold',

    },
    arrow: {
      width: 15,
      height: 15,
      resizeMode: 'contain'
    },
    event: {
      position: 'absolute',
      backgroundColor: '#0000FF',
      opacity: 0.8,
      borderColor: 'black',
      borderWidth: 2,
      borderRadius: 5,
      paddingLeft: 4,
      minHeight: 25,
      flex: 1,
      paddingTop: 5,
      paddingBottom: 0,
      flexDirection: 'column',
      alignItems: 'flex-start',
      overflow: 'hidden',
      ...theme.event
    },
    eventTitle: {
      color: 'black',
      fontWeight: '800',
      fontSize:20,
      minHeight: 15,
      ...theme.eventTitle
    },
    eventSummary: {
      color: 'black',
      fontSize: 22,
      flexWrap: 'wrap',
      fontWeight: 'bold',

      ...theme.eventSummary
    },
    eventTimes: {
      marginTop: 3,
      fontSize: 18,
      fontWeight: 'bold',
      color: 'black',
      flexWrap: 'wrap',
      ...theme.eventTimes
    },
    line: {
      height: 1,
      position: 'absolute',
      left: leftMargin,
      backgroundColor: 'black',
      ...theme.line
    },
    lineNow: {
      height: 1,
      position: 'absolute',
      left: leftMargin,
      backgroundColor: 'red',
      ...theme.line
    },
    timeLabel: {
      position: 'absolute',
      left: 15,
      color: 'black',
      fontSize: 15,
      fontFamily: 'Helvetica Neue',
      fontWeight: '500',
      ...theme.timeLabel
    }
  }
  return StyleSheet.create(style)
}
