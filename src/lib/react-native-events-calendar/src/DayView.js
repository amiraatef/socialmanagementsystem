// @flow
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,Modal,TextInput,StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Meteor, { withTracker, MeteorListView , } from 'react-native-meteor';
import shortid  from 'shortid'

import {Reservation} from '../../../Collection/index'

import populateEvents from './Packer'
import React from 'react'
import moment from 'moment'
import _ from 'lodash'
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';

const LEFT_MARGIN = 60 - 1
// const RIGHT_MARGIN = 10
const CALENDER_HEIGHT = 2400
// const EVENT_TITLE_HEIGHT = 15
const TEXT_LINE_HEIGHT = 17
// const MIN_EVENT_TITLE_WIDTH = 20
// const EVENT_PADDING_LEFT = 4

function range (from, to) {
  return Array.from(Array(to), (_, i) => from + i)
}
 class DayView extends React.PureComponent {
  constructor (props) {
    super(props)
    const width = props.width - LEFT_MARGIN
    const packedEvents = populateEvents(props.events, width)
    let initPosition = _.min(_.map(packedEvents, 'top')) - CALENDER_HEIGHT / 24
    initPosition = initPosition < 0 ? 0 : initPosition
    this.state = {
      _scrollY: initPosition,
      packedEvents,
      lastPress:0, Notes: "",
      Title: "",
      EventDateFormateStart: '',
      EventDateFormateEnd: '',
      status: 'pendding',
      changrcolor:false,
      modalVisible:false,
      StartDate:"",
      StartTime: "",
      End: "",
      isDateTimePickerVisible: false,
    }
  }

  _showDateTimePicker = () => {
  if(this.props.app.Visible==true)
{
  this.props.ChangeMosaleState(false)
}
else{
  this.props.ChangeMosaleState(true)

}
  }
  submit = () => {

    console.log("juju",this.props.app.From,this.props.app.To)
    let Start= moment(this.state.EventDateFormateStart).format("hh:mm:ss a")
    let End=moment(this.state.EventDateFormateEnd).format("hh:mm:ss a")
    let FromTime= moment(this.props.app.From).format("hh:mm:ss a")
    let ToTime= moment(this.props.app.To).format("hh:mm:ss a")

    let FromTimemoment=  moment(FromTime, 'hh:mm a')
    let ToTimemoment=  moment(ToTime, 'hh:mm a')
  let startmoment=  moment(Start, 'hh:mm a')
  let Endmoment=  moment(End, 'hh:mm a')


console.log("startmoment",startmoment)
console.log("Endmoment",Endmoment)
   console.log("FromTimeDB",FromTimemoment)
console.log("ToTimeDB",ToTimemoment)


  const ProviderTimes=Reservation.find( {ProviderID: this.props.app.providerId} )






  for ( var Day in this.props.app.Days)
  {
    // console.log( moment(this.state.EventDateFormateStart).format("dddd"), this.props.app.Times[time])
    if(moment(this.state.EventDateFormateStart).format("dddd")==this.props.app.Days[Day])
    {
      this.setModalVisible(false)
      this.setState({changrcolor:true})   
      alert(`${this.props.app.Days[Day]}.....is not available
      Please choose another Day 
      `)
 return
    }
  }


  if((startmoment.isBetween(FromTimemoment,ToTimemoment))||((Endmoment.isBetween(FromTimemoment,ToTimemoment))))
  {
    this.setModalVisible(false)
    this.setState({changrcolor:true})   

    alert(`please Choose Time in between this Times ${FromTime}::${ToTime}`)
    return
  } 
  
  for(var busytime in ProviderTimes)
  {
    console.log("moment(ProviderTimes[busytime].Event.start",moment(ProviderTimes[busytime].Event.start))

 
console.log("inloop",moment(ProviderTimes[busytime].Event.start),moment(ProviderTimes[busytime].Event.end),Start)
if(startmoment.isBetween(moment(ProviderTimes[busytime].Event.start),moment(ProviderTimes[busytime].Event.end)||
endmoment.isBetween(moment(ProviderTimes[busytime].Event.start),moment(ProviderTimes[busytime].Event.end)
)))
{    this.setModalVisible(false)

  alert("Times are taken before")
return
}

  }


      if (Meteor.user().userType == 'Customer') {
        this.setState({ status: "Pendding" })
     }
 
     this.setModalVisible(false)
 
    
 
 
 
     var reservationId=shortid.generate()
     var notificationId=shortid.generate()
 
     let BookingDB = {
       reservationId:reservationId,
       userId: Meteor.userId(),
       Event: {
         start: this.state.EventDateFormateStart,
         end: this.state.EventDateFormateEnd,
         Notes: this.state.Notes,
         title:this.state.Title
       },
       status: this.state.status,
       ProviderID:this.props.app.providerId,
     }
     let EventUI = {
       start: this.state.EventDateFormateStart,
       end: this.state.EventDateFormateEnd,
       title: this.state.status,
       summary: this.state.Notes
     }
 
     console.log("objDB", BookingDB)
 this.props.MoveEvent(EventUI)
 
 
    Meteor.call("addReservation",(BookingDB) )
 
   let FromName= Meteor.user().profile.UserName
    let Notificationobj={
     notificationId:notificationId,
     RseservationId:reservationId,
      FromuserID: Meteor.userId(),
      FromuserName :FromName,
      TouserID:this.props.app.ProviderID,
      status:"New",
      NotificationType:"Booking",
      content:`there is a reservation from ${FromName} 
      From  ${this.state.EventDateFormateStart}
      To  ${this.state.EventDateFormateEnd}
      please accept or reject`
    }
 
 
 
    Meteor.call("addNotification",(Notificationobj) )

    

  


  
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  _hideDateTimePicker = () => { 
    
    this.props.ChangeMosaleState( false)
  
  };


  onPress= ()=> {
    var delta = new Date().getTime() - this.state.lastPress;
console.log( this.state.lastPress)
this.setState({
  Title:"",
  End:"",
  Notes:"",
  StartDate:""

})
this.setModalVisible(true)
      // double tap happend
      this.props.isDbClicked(true)

    this.setState({
      lastPress: new Date().getTime()
    })
    console.log( this.state.lastPress)
    //changeinstore
  }

  componentWillReceiveProps (nextProps) {
    const width = nextProps.width - LEFT_MARGIN
    this.setState({
      packedEvents: populateEvents(nextProps.events, width)
    })
  }

  componentDidMount () {
    this.props.scrollToFirst && this.scrollToFirst()
  }

  scrollToFirst () {
    setTimeout(() => {
      if (this.state && this.state._scrollY && this._scrollView) {
        this._scrollView.scrollTo({ x: 0, y: this.state._scrollY, animated: true })
      }
    }, 1)
  }

  _renderRedLine() {
      const offset = CALENDER_HEIGHT / 24
      const { format24h } = this.props
      const { width, styles } = this.props
      const timeNowHour = moment().hour()
      const timeNowMin = moment().minutes()
      return (
          <View key={`timeNow`}
            style={[styles.lineNow, { top: offset * timeNowHour + offset * timeNowMin / 60, width: width - 20 }]}
          />
    )
  }

  _renderLines () {
    const offset = CALENDER_HEIGHT / 24
    const { format24h } = this.props

    return range(0, 25).map((item, i) => {
      let timeText
      if (i === 0) {
        timeText = ``
      } else if (i < 12) {
        timeText = !format24h ? `${i} AM` : i
      } else if (i === 12) {
        timeText = !format24h ? `${i} PM` : i
      } else if (i === 24) {
        timeText = !format24h ? `12 AM` : 0
      } else {
        timeText = !format24h ? `${i - 12} PM` : i
      }
      const { width, styles } = this.props
      return [
        <Text onPress={this.onPress}
          key={`timeLabel${i}`}
          style={[styles.timeLabel, { top: offset * i - 6 }]}
        >
          {timeText}
        </Text>,
        i === 0 ? null : (
          <View 
            key={`line${i}`}
            style={[styles.line, { top: offset * i, width: width - 20 }]}
          />
        ),

        <View 
          key={`lineHalf${i}`}
          style={[styles.line, { top: offset * (i + 0.5), width: width - 20 }]}
        />

      ]
    })
  };

  _renderTimeLabels () {
    const { styles } = this.props
    const offset = CALENDER_HEIGHT / 24
    return range(0, 24).map((item, i) => {
      return (
        <View key={`line${i}`} style={[styles.line, { top: offset * i }]} />
      )
    })
  }

  _onEventTapped (event) {
    this.props.eventTapped(event)
  };

  _renderEvents () {
    const { styles } = this.props
    const { packedEvents } = this.state
    let events = packedEvents.map((event, i) => {
      const style = {
        left: event.left,
        height: event.height,
        width: event.width,
        top: event.top
      }

      // Fixing the number of lines for the event title makes this calculation easier.
      // However it would make sense to overflow the title to a new line if needed
      const numberOfLines = Math.floor(event.height / TEXT_LINE_HEIGHT)
      const formatTime = this.props.format24h ? 'HH:mm' : 'hh:mm A'
      return (
        <View
          key={i}
          style={[styles.event, style]}
        >
          {this.props.renderEvent ? this.props.renderEvent(event) : (
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => this._onEventTapped(this.props.events[event.index])}
            >
              <Text 
               onPress={()=>{this.onPress}}
              numberOfLines={1} style={styles.eventTitle}>{`Title: ${event.title}` || 'Event'}</Text>
              {numberOfLines > 1
                ? <Text
                  numberOfLines={numberOfLines - 1}
                  style={[styles.eventSummary]}
                >
                  {`Notes: ${event.summary}` || ' '}
                </Text>
                : null}
              {numberOfLines > 2
                ? <Text 
                onPress={()=>{this.onPress}}
                style={styles.eventTimes} numberOfLines={1}>{`From: ${moment(event.start).format(formatTime)} To: ${moment(event.end).format(formatTime)}`}</Text>
                : null}
            </TouchableOpacity>
          )}
        </View>
      )
    })

    return (
      <View>
        <View style={{ marginLeft: LEFT_MARGIN }}>
          {events}
        </View>
      </View>
    )
  }
  _handleDatePicked = (date) => {
    EventdateFormate = moment(date).format('MM-DD-YYYY HH:mm:ss')
    var pickedTimeFormate = moment(EventdateFormate).format("hh:mm a")
    var pickedDateFormate = moment(EventdateFormate).format("MM-DD-YYYY")


    // console.log('A EventdateFormate: ', EventdateFormate);
    // console.log('A pickedDateFormate: ', pickedDateFormate);
    // console.log('A pickedTimeFormate: ', pickedTimeFormate);

    if (this.state.StartDate) {
      this.setState({ End: EventdateFormate ,EventDateFormateEnd:EventdateFormate})
      // this.setState({ EventDateFormateEnd: EventdateFormate })
      // console.log("EventEnd", EventDateFormateEnd)
      // alert("Close")
    
      this.props.ChangeMosaleState( false)
    
    // console.log("VisibleClose",this.props.app.Visible)
    }
    else {
      // alert("setstateStart")
      this.setState({ StartDate: EventdateFormate,EventDateFormateStart:EventdateFormate })
      // this.setState({ EventDateFormateStart: EventdateFormate })
      // console.log("EventEnd", EventDateFormateStart)
      // alert("Close")
    
      this.props.ChangeMosaleState( false)
    
    }

    this._hideDateTimePicker();

  }
  render () {
  
   let newstyle={color:'green',fontWeight:"500"}
   if(this.state.changrcolor)
   {
    newstyle={color:'red',fontWeight:"700"}
   }

  let Timeson= this.props.app.Days.map(time=>{
     return(<Text>{time}</Text>)
   })
    const { styles } = this.props
    return (
      <View>
        <View>
              
<Modal
          presentationStyle="pageSheet"
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
          }}
       >
        {console.log("Modal")}
          <ScrollView  >
            <View style={Styles.container}>

              <TouchableOpacity style={Styles.section}>
              <Icon style={{ marginRight: 10 }} name="comments" size={30} color="#900" onPress={this._showDateTimePicker}/>
                <TextInput
                  style={Styles.inputContainer}
                  onChangeText={(Title) => this.setState({ Title })}
                  multiline
                  value={this.state.Title}
                  placeholder="Title"
                >
                </TextInput>
              </TouchableOpacity>


              <TouchableOpacity style={Styles.section}>

                <Icon style={{ marginRight: 10 }} name="calendar-alt" size={30} color="#900" 
onPress={()=>this._showDateTimePicker}
            // onPress={this._showDateTimePicker}
                />
                
      <TextInput style={Styles.inputContainer}
                  onFocus={this._showDateTimePicker}
                  value={this.state.StartDate}
                  placeholder="From"
                  underlineColorAndroid='transparent'
                >

                </TextInput>
              </TouchableOpacity>



              <View style={Styles.section}>
                <Icon name="calendar-alt" size={30} color="#900" onPress={()=>this._showDateTimePicker} 
          style={{ marginRight: 10 }} />

       <TextInput style={Styles.inputContainer}
                  onFocus={this._showDateTimePicker}
                  value={this.state.End}
                  placeholder="To"
                  underlineColorAndroid='transparent'
                >
                </TextInput>
              </View>
              <View style={Styles.section}>
                <Icon name="newspaper" size={30} color="#900" style={{ marginRight: 10 }} />
                <TextInput
                  style={Styles.Notes}
                  onChangeText={(Notes) => this.setState({ Notes })}
                  multiline
                  value={this.state.Notes}
                  placeholder="Notes"
                />
              </View>

              <DateTimePicker
                isVisible={this.props.app.Visible}
                onConfirm={(dateTime) =>{
                  this._hideDateTimePicker
                  this._handleDatePicked(dateTime)}}
                onCancel={this._hideDateTimePicker}
                mode="datetime"
                is24Hour={false}

              />
            <View>
            <Text style={{color:"red",fontSize:20}}
            >All Days Except on ....</Text>
            {Timeson}
            <Text style={{color:"red",fontSize:20}}>From: {moment(this.props.app.From).format("hh:mm:ss a")} To:{moment(this.props.app.To).format("hh:mm:ss a")}</Text>
              </View>
              <TouchableOpacity style={Styles.buttonContainer}>
                <Text style={Styles.buttonText} onPress={() => {
                  this.submit()
                }}>Submit</Text>
              </TouchableOpacity>
              <TouchableOpacity style={Styles.buttonContainer}>
                <Text style={Styles.buttonText} onPress={() => { this.setState({modalVisible:false})
                }}>Cancel</Text>
              </TouchableOpacity>
            </View>

          </ScrollView>
        </Modal>
          </View>
          <View>
      <ScrollView ref={ref => (this._scrollView = ref)}
        contentContainerStyle={[styles.contentStyle, { width: this.props.width }]}
      >
        {this._renderLines()}
        {this._renderEvents()}

        {/* {this._renderRedLine()} */}
      </ScrollView>
      </View>
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {

    ...state

  }
}
const mapDispatchersToProps = (dispatcher) => {
  return {
    isDbClicked: (DBclick) => dispatcher({ type: 'dbClicked', value: DBclick }),
    MoveEvent :(event)=>dispatcher({type:'addevent',value:event}),
    ChangeMosaleState :(visible)=>dispatcher({type:'ModalState',value:visible}),


}}
const tracker = withTracker(props => {//---------->changed 
  const reservationshandel = Meteor.subscribe('reservations')
  const Times= Reservation.find( { $or: [ {RecieverID: Meteor.userId() }, {ProviderID: Meteor.userId() } ] })
 

  return {
    reservationshandelLoading: !reservationshandel.ready(),
    UIEventList:Times
  }
})(DayView)
export default connect(mapStateToProps, mapDispatchersToProps)(tracker)

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  Logo: {
    color: 'white',
    fontSize: 40
    ,
    fontStyle: 'italic',
    fontWeight: 'bold',
    textShadowColor: '#252525',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 15,
    marginBottom: 20
  },

  inputContainer: {
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 50,
    backgroundColor: 'rgba(255,255,255,0.2)',
    width: 300,
    height: 40,
    padding: 10,
    fontSize: 20
  },
  Notes: {
    fontSize: 22,
    padding: 10,
    width: 300,

    marginBottom: 10,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: 'black',


  },
  section: {
    flex: 1,
    flexDirection: 'row',
    padding: 10
  }, buttonContainer: {
    alignSelf: 'stretch',
    margin: 20,
    padding: 20,
    backgroundColor: 'yellow',
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: 'rgb(240,230,140)'


  }
  ,
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center'
  }

})