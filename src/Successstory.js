import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Select, Option } from 'react-native-select-lists';
import Draggable from './lib/react-native-draggable';
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';
import ImagePicker from 'react-native-image-picker';
import { Card, ListItem, Button, Rating } from 'react-native-elements'
import Holder from './lib/react-native-draggable-holder'
import Meteor, { withTracker, MeteorListView, } from 'react-native-meteor';
import ReactNative, {
  AppRegistry,
  findNodeHandle,
  StyleSheet,
  UIManager,
  ScrollView,
  Text,
  View,
  PixelRatio,
  TouchableOpacity,
  Image,
  Platform,
  TextInput,
  BackHandler
} from 'react-native';
export default class SucessStory extends Component {
  constructor() {
    super()
    this.Measure = this.Measure.bind(this);
  }
  state = {

    ImageSource: null,
    isOpen: false,
    Type: "",
    URL: "",
    text: "",
    postionImage: {
      x: 0,
      offsetX: 0,
      offsetY: 0,
      y: 0
    },
    postionText: {
      x: 0,
      offsetX: 0,
      offsetY: 0,
      y: 0
    }
  };
  onDrawerSnap(event) {

  }
  onDragEvent(event) {
    console.log(event)
    console.log(event.nativeEvent.targetSnapPointId)
  }
  selectPhotoTapped() {

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      cameraType: 'front',
      mediaType: 'mixed',
      allowEditing: true,
      storageOptions: {
        skipBackup: true,
        cameraRoll: true
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
//        let source = { uri: response.uri };

        // console.log("source", source)
        // You can also display the image using data:
         let source = { uri: 'data:image/jpeg;base64,' + response.data };
          //let source =  response.data ;
    Meteor.call('Converttoonline',source,(error,result)=>{
 this.setState({

  ImageSource: result

});
console.log("result=",result)



      })        
      }
    });
  }


  Measure = () => {
    this.refs.Quote.measure((x, y, width, height, px, py) => {
console.log("measer text",x, y, width, height, px, py)
      this.setState(prevState => ({
        postionText: {
          ...prevState.postionText,
          x: x,
          offsetX: px,
          offsetY: py,
          y: y
        }
      }))
    })

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  handleBackPress = () => {
    this.props.navigation.navigate('Home', { name: 'Jane' }) // works best when the goBack is async
    return true;
  }
  render() {
    
   const MeasureImage = () => {
      this.refs.image.measure((x, y, width, height, px, py) => {
        console.log("measer image",x, y,px,py)

        this.setState(prevState => ({
          postionImage: {
            ...prevState.postionImage,
            offsetX: px,
            offsetY: py,
            x: x,
            y: y
          }
        }))
      })

    }

    const { navigate } = this.props.navigation;
    const addNugget = () => {
      this.Measure()
      MeasureImage()
      let NuggetDB = {
        Type: this.props.Nuggettype,
        URL: this.state.ImageSource,
        text: this.state.text,
        postionImage: {
          x: this.state.postionImage.x,
          y: this.state.postionImage.y,
          offsetX:this.state.postionImage.offsetX,
          offsetY:this.state.postionImage.offsetY
        },
        postionText: {
          x: this.state.postionText.x,
          y: this.state.postionText.y,
          offsetX:this.state.postionText.offsetX,
          offsetY:this.state.postionText.offsetY
        },
        ProviderName: Meteor.user().profile.UserName
      }
console.log("NuggetDB",NuggetDB)

      this.props.navigation.navigate('LimitTimesComponent', NuggetDB)
    }
    return (
      <View>
        <Icon name="upload" size={30} color="#900" onPress={this.selectPhotoTapped.bind(this)} />

        <Holder
          pressDragRelease={this.Measure} reverse={false} >
          <TextInput ref="Quote"
            style={styles.Quote}
            multiline={true}
            onChangeText={(text) => this.setState({ text })}
          />
        </Holder>
        <Holder  reverse={false} pressDragRelease={MeasureImage} renderShape="image" imageSource={null} >
      
          <Image style={{ width: 300, height: 300 }} ref="image" source={{uri:this.state.ImageSource}} />
        </Holder>
        {/* <Button style={styles.btn} title="Save" onPress={addNugget} /> */}

        <Button style={styles.btn} title="Choose From Gallery" onPress={() => {
          this.props.navigation.navigate('Gallery', { name: 'Jane' })
        }} />


        <Button style={styles.btn} title="Continue" onPress={addNugget} />

      </View>
    )
  }
}
const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF8E1',
    flexWrap: 'wrap'
  },

  ImageContainer: {
    borderRadius: 10,
    width: 80,
    height: 80,
    borderWidth: 1,
    alignItems: 'center',
    backgroundColor: '#337DFF',
    borderColor: '#fff',
    fontStyle: 'italic',
    fontWeight: 'bold',
    fontSize: 16,

    textShadowColor: '#252525',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 15,
  },
  bottomView: {

    width: '100%',
    height: 50,
    backgroundColor: '#FF9800',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0
  },
  Quote: {
    fontStyle: 'italic',
    fontWeight: 'bold',
    borderColor: 'black',
    fontSize: 16,
    borderWidth: 5,
    height: 50,
    width: 150,
    borderRadius: 10,

  },
  btn: {
    color: 'white',
    flexWrap: 'wrap',
    flexDirection: 'row',
    fontSize: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 5,
    borderRadius: 10,


  }
});