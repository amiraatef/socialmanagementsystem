import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    BackHandler
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'
import { MyNotification, Reservation } from './Collection/index'
import Meteor, { withTracker, MeteorListView } from 'react-native-meteor'
import React, { Component } from 'react'
import shortid  from 'shortid'
import {Button} from 'react-native-elements'

class myNotifications extends Component {
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }
    componentWillUnmount() {
         BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
      }
      sendnotification(notification,response)
      {
        var notificationId=shortid.generate()

        let FromName= Meteor.user().profile.UserName
console.log("Response",response)
          if(response=="Reject")
          {
              console.log("rejectednotification",notification)
Meteor.call("cancellReservationDeleteNotification",notification)

let Notificationobj={
    notificationId:notificationId,
    RseservationId:notification.RseservationId,
     FromuserID: Meteor.userId(),
     FromuserName :FromName,
     TouserID:notification.FromuserID,
     status:"New",
     NotificationType:"Response",
     content:` reservation has been cancelled by ${FromName} please choose another appointment and this reservation autmatically has been deleted from your calender 
     `
   }
   console.log("rejectednotification",Notificationobj)

Meteor.call("addNotification",Notificationobj)

          }
          else{
            Meteor.call("AcceptReservationDeleteNotification",notification)
            let Notificationobj={
                notificationId:notificationId,
                RseservationId:notification.RseservationId,
                 FromuserID: Meteor.userId(),
                 FromuserName :FromName,
                 TouserID:notification.FromuserID,
                 status:"New",
                 NotificationType:"Response",
                 content:` reservation has been accpted by ${FromName} please be on time 
                 `
               }
               Meteor.call("addNotification",Notificationobj)

          }
      }
  
    render() {
        
        let itemList, NotificationlistEmpty = null
     let type=false
     console.log(Meteor.user().profile.UserType)
   if( Meteor.user().profile.UserType==="Thripest")
   {
type=true
   }

        console.log("lolo",Meteor.user().UserType)
        if (this.props.Notificationlist.length) {
            itemList = this.props.Notificationlist.map((notification) => {
                let ICON = ""

                if (notification.NotificationType == "Booking") {
                    ICON = 'calendar-alt'
                }
                else{
                    ICON = 'calendar-alt'

                }
                return (<View style={styles.mainContainer}   >
                    <View >
                    <Icon name={ICON} size={30} color="green" />
                        <Text style={styles.text}>{notification.content}</Text>
                    </View>
                 {type &&  <View style={styles.BTNVIEW}>
                        <Button onPress={()=>{this.sendnotification(notification,"Accept")
                        }} title="Accept" style={styles.BtnNotifyAccept}  />
                        <Button onPress={()=>{this.sendnotification(notification,"Reject")
                        }}  title="Reject" style={styles.BtnNotifyReject} />
                        </View>
                 }
                </View>
                )

            })

        }
        else {
            NotificationlistEmpty = 'No Notification until now'
        }
        console.log(itemList)

        return (<ScrollView>
            <View>
            {itemList || <Text>{NotificationlistEmpty}</Text>}
            </View>
            
        </ScrollView>

        )
    }
}
const notify= withTracker(props => {//---------->changed 
    const Mynotificationrhandle = Meteor.subscribe('Mynotification')
    const List = MyNotification.find({ TouserID: Meteor.userId() })
console.log("List",List)
    return {
        Notificationlist: List
    }
})(myNotifications)
export default notify
const styles = StyleSheet.create({

    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between', alignItems: 'center',
        borderBottomWidth: 1
    },
    img: {
        borderRadius: 100,
        width: 70,
        height: 70

    },
    username: {
        fontWeight: 'bold',
        fontSize: 26,

    },
    text: {
        fontSize: 22,
        flexDirection: 'row',
        paddingLeft: 30,

    },
    section: {
        flex: 1,
        flexDirection: 'row',
        padding: 10
    },
    BtnNotifyReject:{
        backgroundColor:"red",
     color:"white",
        width:50,
        height:30,
        borderRadius:5,
        fontWeight: 'bold',
        marginRight:50
       
    },
    BtnNotifyAccept:{
        backgroundColor:"green",
     color:"white",
        width:50,
        height:30,
        borderRadius:5,
        fontWeight: 'bold',
       
    },
    BTNVIEW:{
        flexDirection: 'row',
        justifyContent: 'space-around',

    }
   

})