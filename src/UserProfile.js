import React, { Component } from 'react'
import { View , Text , ScrollView , StyleSheet,BackHandler} from 'react-native'
import Bar from './Bar'
import Header from './Header'
import PhotoGrid from './PhotoGrid'
export default class ProfilePage extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  // componentWillUnmount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  // }
  
  handleBackPress = () => {
    console.log("hd",this.props.navigation)
    this.props.navigation.navigate('Home', { name: 'Jane' }) // works best when the goBack is async
    return true;
  }
  render() {
    return (
        <View style={styles.container}>
        <ScrollView>
<Header/>
<Bar/>
<View>
<PhotoGrid/>
</View>
</ScrollView>
</View>
    )
  }
}
const styles = StyleSheet.create( {

container:{
    flex:1,
    backgroundColor:"#000"
}

})