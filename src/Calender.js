import React from 'react'
import { Dimensions, StyleSheet, TouchableHighlight, View, TouchableNativeFeedback,BackHandler, TouchableOpacity, ImageBackground, ScrollView, Text, TextInput, Modal, label } from 'react-native'
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import moment from 'moment';
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';
import Icon from 'react-native-vector-icons/FontAwesome5'
import DoubleClick from 'react-native-double-click';
import Meteor, { withTracker, MeteorListView, } from 'react-native-meteor';
import { connect } from 'react-redux';
import{Reservation}from './Collection/index'

import DateTimePicker from 'react-native-modal-datetime-picker';

import EventCalendar from './lib/react-native-events-calendar'

let { width } = Dimensions.get('window')
class Calender extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      Notes: "",
      Title: "",
      EventDateFormateStart: '',
      EventDateFormateEnd: '',
      status: '',

      modalVisible: false,
      start: '',
      StartTime: "",
      End: '',
      isDateTimePickerVisible: false,
      events: [
        /* { start: '2018-08-22 00:30:00', end: '2018-08-22 01:30:00', title: 'Dr. Mariana Joseph', summary: '3412 Piedmont Rd NE, GA 3032' },
         { start: '2018-08-23 01:30:00', end: '2017-08-23 02:20:00', title: 'Dr. Mariana Joseph', summary: '3412 Piedmont Rd NE, GA 3032' },
         { start: '2018-08-24 04:10:00', end: '2017-081-24 04:40:00', title: 'Dr. Mariana Joseph', summary: '3412 Piedmont Rd NE, GA 3032' },*/
      ],
      lastPress: 0
    };

  }

  _eventTapped(event) {
    alert(JSON.stringify(event))
  }

// componentDidMount()
// {
//   console.log("componentDidMount")
//   Meteor.call("GetReservation",(err,result)=>{
//     if (err)
//     {
//       console.log("err",err)
//     }
//     else{
//       console.log("result",result)
//       CurrentEvent=[...result]
//       console.log("CurrentEvent",CurrentEvent)
// this.props.MoveEvent(result)
//     }
//   })
  
  

// }





  
  render() {
    var currentDate = moment().format("YYYY-MM-DD");
    return (
      <TouchableNativeFeedback>
    
        <EventCalendar
          eventTapped={this._eventTapped.bind(this)}
          events={this.props.UIEventList}
          width={width}
          numberOfDay={60}
          initDate={currentDate}
          scrollToFirst
        />
      </TouchableNativeFeedback>

    )
  }
}
const mapStateToProps = (state) => {
  return {

    ...state

  }
}
const mapDispatchersToProps = (dispatcher) => {
  return {
    isDbClicked: (DBclick) => dispatcher({ type: 'dbClicked', value: DBclick }),
    MoveEvent :(event)=>dispatcher({type:'addevent',value:event}),

  }
}
const tracker = withTracker(props => {//---------->changed 
  const reservationshandel = Meteor.subscribe('reservations')
  const Times= Reservation.find( { $or: [ {RecieverID: Meteor.userId() }, {ProviderID: Meteor.userId() } ] })
  let UIArray=[]
        Times.map((item)=>{
          let event={
            start:item.Event.start,
            end:item.Event.end,
            title:item.Event.title,
            summary:`${item.Event.Notes}Status:${item.status}`,

          }
    UIArray.push(event)
        })
  return {
    reservationshandelLoading: !reservationshandel.ready(),
    UIEventList:UIArray
  }
})(Calender)
export default connect(mapStateToProps, mapDispatchersToProps)(tracker);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  Logo: {
    color: 'white',
    fontSize: 40
    ,
    fontStyle: 'italic',
    fontWeight: 'bold',
    textShadowColor: '#252525',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 15,
    marginBottom: 20
  },

  inputContainer: {
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 50,
    backgroundColor: 'rgba(255,255,255,0.2)',
    width: 300,
    height: 40,
    padding: 10,
    fontSize: 20
  },
  Notes: {
    fontSize: 22,
    padding: 10,
    width: 300,

    marginBottom: 10,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: 'black',


  },
  section: {
    flex: 1,
    flexDirection: 'row',
    padding: 10
  }, buttonContainer: {
    alignSelf: 'stretch',
    margin: 20,
    padding: 20,
    backgroundColor: 'yellow',
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: 'rgb(240,230,140)'


  }
  ,
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center'
  }

})
