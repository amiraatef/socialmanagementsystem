import React, { Component } from 'react'
import { View, Text, Footer, FooterTab, Button,Container, Content, Header } from 'native-base';
import { StyleSheet, ScrollView } from 'react-native'
import Headercmp from './Header'
import { Card, ListItem, Rating } from 'react-native-elements'
import Eventscmp from './Events'
import Products from './Product'
import Icon from 'react-native-vector-icons/MaterialIcons'

// import Footer from './Footer'
export default class Home extends Component {
    render() {
        return (
            <Container>
                 <Headercmp />
                <Content >
                    <ScrollView >
                        <Eventscmp />
                        <Products />

                    </ScrollView>
                </Content>
                {/* < Footer/> */}
                <Footer >
                    <FooterTab   style={{backgroundColor:"#FFF"}}>
                        <Button style={{paddingLeft:0, paddingRight:0,borderRadius:100}}>
                            <Icon size={30} name="navigation" style={{color:"#65c670" ,borderRadius:100}} />
                        </Button>
                        <Button  active style={{backgroundColor:"white"}}> 
                            <Icon size={30} name="comment"  />
                        </Button>
                        <Button  style={{color:'#cdcdcd',backgroundColor:"#65c670",borderRadius:100}}  >
                            <Icon size={30} active name='add' />
                        </Button>
                        <Button   style={{color:'#cdcdcd',backgroundColor:"white"}}  >
                            <Icon size={30} active name="spa" />
                        </Button>
                        <Button >
                            <Icon size={30} name="person" />
                        </Button>

                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    EventView: {
        flex: 1,
        backgroundColor: '#65c670',
        flexDirection: 'row',
        height: 150,
        width: '100%',
        marginLeft: 10

    },
    Rating: {
        color: "#7f7f7f",
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    Location: {
        color: "#434343",
        fontWeight: "bold",
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10

    }

})