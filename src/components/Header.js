import React, { Component } from 'react'
import { View, Container, Text, Header, Icon, Left, Right, } from 'native-base';
import { StyleSheet } from 'react-native'
export default class Headercmp extends Component {


state={
Clicked:false,
Feed:"Feed",
Discover:"Discover"

}


    render() {
       const changestlyes=(word)=>{
if(word){
    this.setState((prevState)=>({
        
      Feed:prevState.Feed==="Feed"?"Discover":"Feed",
      Discover:prevState.Discover==="Discover"?"Feed":"Discover"
    }))
}
           this.setState((prevState)=>({
               Clicked:!prevState.Clicked
           }))
       }




        return (
            <View>
                <Header androidStatusBarColor="white" style={styles.header}>
                    <Left>
                        <View style={styles.routingwords}>
                            <Text onPress={()=>{
                                changestlyes('Feed')
                            }} style={styles.wordstylePrim} >{this.state.Feed}</Text>
                            <Text style={styles.wordstylesec} onPress={()=>{
                                changestlyes("Discover")
                            }}>{this.state.Discover}</Text>
                          

                        </View>
                    </Left>
                    <Right>
                        <View style={styles.icons_}>
                            <Icon name='search' />
                            <Icon name='cart' />
                        </View>
                    </Right>
                </Header>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',

    },
    routingwords: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        flex: .7,
        width: 200,
    },
    wordstylePrim:{
        fontSize:22,
        fontWeight:'bold',
        color:'#434343',
        alignSelf:'flex-start',
        borderBottomWidth:1,
        borderBottomColor:"#65c670"
    },
    wordstylesec: {
        fontWeight:'bold',
            fontSize:16,
            color:'#bebebe',
            alignSelf:'flex-end'
    },
  
    icons_: {
        justifyContent: 'space-around',
        flexDirection: 'row',
        flex: .3
    }

})