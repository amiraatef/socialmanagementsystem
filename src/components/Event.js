import React, { Component } from 'react'
import { View, Text } from 'native-base';
import { StyleSheet, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

import { Card, ListItem, Button, Rating } from 'react-native-elements'
export default class Event extends Component {
   
  render() {
    return (
        <Card>
    <View style={styles.EventView}>
    
        <Text style={{ fontWeight: "bold", fontSize: 28, color: 'white', padding: 10 }}>

        Yoga Node in Nill
        

        </Text>
        <View style={{backgroundColor:"#d0d0d0",width:30,height:30}}>
    <Icon size={30}  name="bookmark"/>
    </View>
        </View>
        <View style={styles.Rating}>
        <Text>By:Mohammed Ali</Text>
        <Rating imageSize={20}
            type="custom" startingValue={3.6} ratingColor="white" ratingBackgroundColor="#7f7f7f" />
        <Text>40 people will going </Text>
        
        </View>
        <View style={styles.Location}>
        <Text style={{ fontWeight: "bold" }}>Garden City,Cairo</Text>
        
        <Text style={{ fontWeight: "bold" }}>2-5 July</Text>
        <Text style={{ fontWeight: "bold" }}>$400</Text>
        
        
        </View>
        </Card>
    )
  }
}
const styles = StyleSheet.create({
    EventView: {
        flex: 1,
        backgroundColor: '#65c670',
        flexDirection: 'row',
        height: 150,
        width:'87%',
        borderRadius:10,justifyContent:'space-between'

    },
    Rating: {
        color: "#7f7f7f",
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    Location: {
        color: "#434343",
        fontWeight: "bold",
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10

    }

})