import React, { Component } from 'react'
import { Card, ListItem, Button, Rating } from 'react-native-elements'
import Meteor, { withTracker, MeteorListView, } from 'react-native-meteor';
import { Nuggets, Reservation, TypesNuggets } from './Collection/index'
import { connect } from 'react-redux';

import {
  Platform,
  StyleSheet,
  Text,
  View, TextInput,
  ScrollView, BackHandler, Image, Modal, TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'

class Posts extends Component {
  state = {
    modalVisible: false,
    NuggetID: null
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  // componentWillUnmount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  // }

  handleBackPress = () => {
    console.log("hd", this.props.navigation)
    this.props.navigation.navigate('Home', { name: 'Jane' }) // works best when the goBack is async
    return true;
  }
  openModal = (visible) => {
    this.setState({ modalVisible: visible })
    alert("jiji")
  }
  render() {

    let SelectedNuggetlist = ""
    console.log("this.state.NuggetID", this.state.NuggetID)
    let SelectedNugget = TypesNuggets.findOne({ NuggetID: this.state.NuggetID })
    console.log("SelectedNugget", SelectedNugget)
    if (SelectedNugget) {
      console.log("SelectedNuggetlist", SelectedNugget.AvailableTimes)

      SelectedNuggetlist = SelectedNugget.AvailableTimes.map((time) => {
        console.log("time", time)
        return (<Text>{time}</Text>
        )
      })
      console.log("SelectedNuggetlist", SelectedNuggetlist)
    }
    let Title = ""
    let ICON = ""
    console.log("Meteor.user().UserType", Meteor.user().profile.UserType)
    if (Meteor.user().profile.UserType == "Customer") {
      Title = 'Book NOW'
      ICON = 'schedule'
    }
    else {
      Title = 'Endorse'
      ICON = 'verified-user'
    }



    let Nuggets = this.props.Nuggetslist.reverse().map((Nugget) => {
      console.log("Nuggetelement", Nugget.URL)
      // let Provider = users.find({ _id: Nugget.userID })
      // console.log("Provider",Provider)
      return (<Card
        title={Nugget.Type}
        image={{ uri: Nugget.URL }}
      >
        {/* <Image style={{width:100,height:100}}  source={{uri:Nugget.URL}}/> */}
        <Text style={{ color: 'blue' }} > {Nugget.ProviderName}
        </Text>
        <Text style={{ marginBottom: 10 }}>
          {
            Nugget.Text
          }  </Text>
        <Button
          icon={{ name: ICON }}
          backgroundColor='#03A9F4'
          fontFamily='Lato'
          buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
          title={Title}
          onPress={() => {
            if (Title == "Book NOW") {
              let SelectedNugget = TypesNuggets.findOne({ NuggetID: Nugget.NuggetID })
              this.props.SetProvider(Nugget.userID)
              console.log(SelectedNugget.From, SelectedNugget.To)
              this.props.setFromTime(SelectedNugget.From)
              this.props.setToTime(SelectedNugget.To)
              this.props.navigation.navigate('Calender', { Times: SelectedNugget.AvailableTimes })
              this.props.StoreDays(SelectedNugget.AvailableTimes)

            }
          }
          }
        />
        <Button
          icon={{ name: ICON }}
          backgroundColor='#03A9F4'
          fontFamily='Lato'
          buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
          title="ShowDetails"
          onPress={() => { this.props.navigation.navigate('Details', Nugget) }}
        />
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', alignContent: 'space-around' }} >
          <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20 }}>
            <Icon size={30} name='heart' color="red" />
            <Text>Like</Text>
          </View>
          <TouchableOpacity onPress={() => this.openModal(true)}
style={{ flex: 1, flexDirection: 'row'}}
          >

            <Icon size={30} name='comments' color="blue" />
            <Text>Comments</Text>

          </TouchableOpacity>
        </View>

       
      </Card>
      )

    })

    return (
      <ScrollView>
        <View>
          {Nuggets}
          <Modal
            presentationStyle="pageSheet"
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
            }}
          >
            <TouchableOpacity style={styles.section}>
              <Icon style={{ marginRight: 10 }} name="heart" size={30} color="#900" />
              <Text>2</Text>
            </TouchableOpacity>
            <Text>list of comments </Text>
            <View style={styles.section} >
              <TextInput
                style={{ borderRadius: 50, width: "100%" ,borderWidth:4,borderColor:"black"}}
                multiline
                placeholde="Write a comment"
              />
            </View>
            <TouchableOpacity onPress={this.openModal(false)} style={styles.section}>
              <Icon  style={{ marginRight: 10 }} name="close" size={30} color="#900" />
            </TouchableOpacity>
          </Modal>
        </View>

      </ScrollView>
    )
  }
}



styles = StyleSheet.create({
  text: {
    fontWeight: 'bold',
    fontSize: 20,

  },
  section: {
    flex: 1,
    flexDirection: 'row',
    padding: 10
  }
})
const mapStateToProps = (state) => {
  return {

    ...state

  }
}
const mapDispatchersToProps = (dispatcher) => {
  return {
    SetProvider: (provider) => dispatcher({ type: 'SelectedProvider', value: provider }),
    StoreDays: (Days) => dispatcher({ type: 'storeDays', value: Days }),
    setFromTime: (time) => dispatcher({ type: 'FromTime', value: time }),
    setToTime: (time) => dispatcher({ type: 'ToTime', value: time }),



  }
}

const tracker = withTracker(props => {//---------->changed 
  const nuggetshande = Meteor.subscribe('nuggets')
  const TypesNugg = Meteor.subscribe('Types')
  return {
    nuggetshandeLoading: !nuggetshande.ready(),
    Nuggetslist: Nuggets.find(),


  }
})(Posts)
export default connect(mapStateToProps, mapDispatchersToProps)(tracker)
