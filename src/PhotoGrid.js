import React, { Component } from 'react'
import { View, Text, ScrollView, StyleSheet, Image, Dimensions } from 'react-native'
import { Card, ListItem, Button, Rating } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'


import Meteor, { withTracker } from 'react-native-meteor';
import { Nuggets } from './Collection/index'
class Postsuser extends Component {
  state = {

    Posts: [
      require('./img/2.jpg'),
      require('./img/3.jpg'),
      require('./img/4.jpg'),
    ]
  }
  render() {

    let Nuggets = this.props.Nuggetslist.map((Nugget) => {

      return (<Card
        title={Nugget.Type}
        image={Nugget.URL.uri}>
        <Text style={{ color: 'blue' }} > {Nugget.ProviderName}
        </Text>
        <Text style={{ marginBottom: 10 }}>
          {
            Nugget.Text
          }  </Text>
          <Button title="Edit"/>
          <Button title="Delete"/>

        {/* <Button
          icon="verified-user"
          backgroundColor='#03A9F4'
          fontFamily='Lato'
          buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
          title="Edit"

        /> */}
        {/* <Button
          icon="verified-user"
          backgroundColor='#03A9F4'
          fontFamily='Lato'
          buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
          title="Delete"
        /> */}
      </Card>
      )

    })

    return (
      <ScrollView>
        <View style={styles.photoGrid}>

          <View style={styles.photoWrap}>
            <ScrollView>
              {Nuggets}
            </ScrollView>

          </View>
        </View>

      </ScrollView>
    )
  }
}
export default withTracker(props => {//---------->changed

  const currentuserhandle = Meteor.subscribe('myuser')
  const nuggetshande = Meteor.subscribe('nuggets')
  return {

    nuggetshandeLoading: !nuggetshande.ready(),
    Nuggetslist: Nuggets.find({ userID: Meteor.userId() }),
    currentUser: Meteor.user(),
  }
})(Postsuser)
const styles = StyleSheet.create({

  // photoGrid: {
  //   flexDirection: 'row',
  //   flexWrap: "wrap",
  // },

  // photoWrap: {
  //   height: 120,
  //   width: '100%',

  // },
  // photo: {
  //   flex: 1,
  //   alignSelf: 'center',
  //   resizeMode: 'cover'
  // }
})
